# yamlselect

This is a basic library to help with selecting nodes from a yaml AST. It
uses my fork of the go-yaml yaml package.

## Installing

go get -u gitlab.com/commondream/yamlselect

## Usage

```
import (
  "github.com/commondream/yaml"
  "gitlab.com/commondream/yamlselect"
)

data := `
foo: bar
baz:
- this
- is
- something
- cool: key
  other: rad
`
node, err := yaml.Parse([]byte(data))
child := yamlselect.SelectNode(node, "baz[3].other")
```

## License

This project is licensed under the MIT license. See LICENSE.md for the full
terms.
