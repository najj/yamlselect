package yamlselect

import (
	"testing"

	"github.com/commondream/yaml"
	"github.com/stvp/assert"
)

func TestSelectKey(t *testing.T) {
	template := "v: 5"
	doc, _ := yaml.Parse([]byte(template))

	selectedNode := SelectNode(doc, "v")
	assert.NotNil(t, selectedNode)
	assert.Equal(t, "5", selectedNode.Value)
}

func TestSelectStringKey(t *testing.T) {
	template := "key: foo"
	doc, _ := yaml.Parse([]byte(template))

	selectedNode := SelectNode(doc, "\"key\"")
	assert.NotNil(t, selectedNode)
	assert.Equal(t, "foo", selectedNode.Value)
}

func TestSelectArray(t *testing.T) {
	template := `
- a
- test
`
	doc, _ := yaml.Parse([]byte(template))

	selectedNode := SelectNode(doc, "[1]")
	assert.NotNil(t, selectedNode)
	assert.Equal(t, "test", selectedNode.Value)
}

func TestComplex(t *testing.T) {
	template := `
foo: bar
baz:
- this
- is
- something
- cool: key
  other: rad
`
	doc, err := yaml.Parse([]byte(template))
	assert.Nil(t, err)

	selectedNode := SelectNode(doc, "baz[3].other")
	assert.NotNil(t, selectedNode)
	assert.Equal(t, selectedNode.Value, "rad")
}

func TestTrailingChars(t *testing.T) {
	template := "v: foo"
	doc, _ := yaml.Parse([]byte(template))

	selectedNode := SelectNode(doc, "v[]")
	assert.Nil(t, selectedNode)
}

func TestTrailingSpace(t *testing.T) {
	template := "v: foo"
	doc, _ := yaml.Parse([]byte(template))

	selectedNode := SelectNode(doc, "v ")
	assert.NotNil(t, selectedNode)
	assert.Equal(t, "foo", selectedNode.Value)
}

func TestNoMatchReturnsNil(t *testing.T) {
	template := "v: baz"
	doc, _ := yaml.Parse([]byte(template))
	selectedNode := SelectNode(doc, "meta")

	assert.Nil(t, selectedNode)
}
